# GitLab Product Movie Club

**2020-09-10**
[Stakeholders, let 'em in](https://www.mindtheproduct.com/stakeholders-building-an-open-door-culture/) - [Discussion](https://www.youtube.com/watch?v=HD1NyW5Jv_Q&feature=youtu.be)

## Future Movie Ideas


| Title	| Speaker | Link |
|------|---------|------|
Product Strategy Means Saying No| Des Traynor |	http://businessofsoftware.org/2014/12/product-strategy-saying-part-2-des-traynor-bos-usa-2014/ | 
| Understanding the Job	| Clayton Christensen| https://www.youtube.com/watch?v=f84LymEs67Y | 
| Uncovering the Jobs to be Done |	Bob Moesta & Chris Spiek |	http://businessofsoftware.org/2014/06/ |bos-2013-bob-moesta-and-chris-spiek-uncovering-the-jobs-to-be-done/ |
| Jobs to be Done: from Doubter to Believer |	Sian Townsend | https://vimeo.com/167029277 |
| The Root Causes of Product Failure |	Marty Cagan	| http://www.mindtheproduct.com/2015/09/video-the-root-causes-of-product-failure-by-marty-cagan/ |
| Outcome-Driven Innovation for Product Managers |	Tony Ulwick| http://www.mindtheproduct.com/2017/01/outcome-driven-innovation-product-managers/ |
| Building Badass Users |	Kathy Sierra |	http://www.mindtheproduct.com/2014/09/kathy-sierra-building-badass-users/ |
| Lean Product Management |	Melissa Perri | https://vimeo.com/122742946 |
| Almost Everything I've Learned From 5 Years of Lean UX | Jeff Gothelf	| https://www.youtube.com/watch?v=qVay1bHUpmc |
| An Introduction to Modern Product Discovery	| Teresa Torres |	https://medium.com/productized-blog/an-introduction-to-modern-product-discovery-by-teresa-torres-productizedconf-bb2703b01fdb |
| Using the Kano Model to Build Dellightful UX |	Jared Spool |	https://www.youtube.com/watch?v=ewpz2gR_oJQ |
| 5 Psychological Principles of Persuasive Product Design |	Nathalie Nahai |	https://vimeo.com/144000578 |
| 10x not 10% - Product management by orders of magnitude |	Ken Norton | 	https://www.kennorton.com/essays/10x-not-10-percent.html |
| Grounding Product in Time	| Scott Belsky |	https://vimeo.com/167866001 |
| Escaping the Build Trap |	Melissa Perri| https://www.mindtheproduct.com/2017/07/escaping-build-trap-melissa-perri/ |
| Future of Enterprise Design |	Amanda Linden | https://youtu.be/z56b1FYR70k |
| 7 Habits for Successful Product Prioritization | Hiten Shah | https://www.youtube.com/watch?v=Upv4RGjw_wQ |
| Adopting Continuous Product Discovery Practices | Teresa Torres |	https://www.youtube.com/watch?v=a2HidFrzYIA |
| Customers Are Not The Source of Innvoation |	Marty Cagan | 	https://businessofsoftware.org/2018/06/why-your-customers/ | 